import Dropdown from "../../atoms/dropdown"

const Header = () => {
    return (
        <div>
            <Dropdown
                title='Projects'
                options={[{title: 'Example 1', id: 228}, {title: 'Example 2', id: 228}]}
            />
        </div>
    )
}

export default Header
