import {useMemo, useState} from "react"
import styles from './Dropdown.module.css'

interface DropdownContentItemInterface {
    title: string,
    id: number
}

interface DropdownInterface {
    title: string,
    options: DropdownContentItemInterface[]
}

const Dropdown = ({title, options}: DropdownInterface) => {
    const [isOpen, setIsOpen] = useState<boolean>(false)

    const handleOpenDropdown = () => {
        setIsOpen(!isOpen)
    }

    const memoOptions = useMemo(() => options.map(item => (<div key={item.id} className={styles.dropdownContentItem}> {item.title} </div>)), [])

    return (
        <div className={`${styles.dropdown} ${isOpen ? styles.active : ''}`} onClick={handleOpenDropdown}>
            <span className={styles.dropdownToggle}> {title} <i className={styles.arrowDown}></i></span>
            <div className={styles.dropdownContent}>
                {
                    memoOptions
                }
            </div>
        </div>
    )
}

export default Dropdown
