import Header from "../molecules/header"
import React, {ReactChild, ReactChildren} from "react"

interface AuxProps {
    children: ReactChild | ReactChildren
}

const MainWrapper = ({children}: AuxProps) => {

    return (
        <div>
            <Header />

            {children}
        </div>
    )
}

export default MainWrapper
