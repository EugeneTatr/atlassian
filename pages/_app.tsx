import type { AppProps } from 'next/app'
import MainWrapper from "../components/Wrappers/MainWrapper"
import '../styles/globals.css'

function MyApp({ Component, pageProps }: AppProps) {
  return (
      <MainWrapper>
        <Component {...pageProps} />
      </MainWrapper>
  )
}

export default MyApp
